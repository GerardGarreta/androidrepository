package com.example.gerardapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    EditText editText;
    String name;
    TextView textView;
    CheckBox checkSelected;
    public List<Persona> personasList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText= findViewById(R.id.getName);
        textView=findViewById(R.id.txtResult);
        checkSelected=findViewById(R.id.chk_select);
        /*Log.v("APP-GERARD", "Bienvneido");

        Persona p = new Persona("Gerard",30);
        p.setNombre("Garret");
        Toast.makeText(this,p.getNombre()+" "+p.getEdad()+" "+ isMayorEdad(p),
                Toast.LENGTH_SHORT).show();
        addPersona();
        isAdultPersonaList();
        */



    }


    public String isMayorEdad(Persona persona){
        if (persona.getEdad() >= 18){
            return ("es mayor de edad");
        }else {
            return ("es menor de edad");
        }
    }

    private static boolean isPrimo(Persona persona) {
            if (persona.getEdad()%2==0) return false;
            for(int i=3;i*i<=persona.getEdad();i+=2) {
                if(persona.getEdad()%i==0)
                    return false;
            }
            return true;
        }

    public void addPersona() {
        Random edadRandom = new Random();
        for (int i=0; i<5; i++){
            personasList.add(new Persona("Persona"+i,edadRandom.nextInt(50 - 17)));
        }
    }

    public void isAdultPersonaList(){
        for(Persona p: personasList){
           Toast.makeText(this,p.getNombre()+" "+p.getEdad()+" "+ isMayorEdad(p)+" "+isPrimo(p), Toast.LENGTH_SHORT).show();
            }
        }

    public void sendAce(View view) {

        name=editText.getText().toString();


            if (!name.equals("")) {
                if (checkSelected.isChecked()) {
                    textView.setText(getString(R.string.saludo1) + " " + name + getString(R.string.saludo2));
                }
                else{
                    textView.setText("Acepta las condiciones");
                }
            }
        }



    public void sendCan(View view) {
        EditText editText= findViewById(R.id.getName);
        TextView textView=findViewById(R.id.txtResult);
        editText.setText("");
        textView.setText("");
    }
}
