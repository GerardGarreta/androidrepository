package com.example.project3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText etName, etEdad;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = findViewById(R.id.getName);
        etEdad = findViewById(R.id.getEdad);
    }

    public void buttonGoPressed(View view) {
        String valor1 = etName.getText().toString();
        int valor2 = Integer.parseInt(etEdad.getText().toString());
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        intent.putExtra("valor_num",valor2);
        intent.putExtra("valor_text", valor1);
        startActivity(intent);
    }
}