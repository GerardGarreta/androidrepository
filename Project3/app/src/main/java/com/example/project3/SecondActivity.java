package com.example.project3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView tv_resultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tv_resultado = findViewById(R.id.txtResult);

        //Recogemos los extras que mandamos desde MainActivity
        int valor_num = getIntent().getIntExtra("valor_num",0);

        String valor_text = getIntent().getStringExtra("valor_text");

        //Modifico el texto del TextView del layout:
        tv_resultado.setText(valor_text+" "+valor_num);
    }

    public void goBackButtonPressed(View view)
    {

        finish();
    }
}