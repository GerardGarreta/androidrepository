package com.example.formulario;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    EditText etName, etPhone, etEmail, etPassword, etDate;
    int pDay, pMonth, pYear = 0;
    Activity activity = this;

    public DatePickerDialog.OnDateSetListener pDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    pYear = year;
                    pMonth = month;
                    pDay = dayOfMonth;

                    String fromDay = Integer.toString(pDay);
                    String fromMonth = Integer.toString(pMonth + 1);

                    if (fromDay.length() == 1) fromDay = "0" + fromDay;
                    if (fromMonth.length() == 1) fromMonth = "0" + fromMonth;

                    etDate.setText(fromDay + "/" + fromMonth + "/" + pYear);
                }
            };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etName = findViewById(R.id.getName);
        etEmail = findViewById(R.id.getEmail);
        etPhone = findViewById(R.id.getTelf);
        etPassword = findViewById(R.id.getPassword);
        etDate = findViewById(R.id.getDate);

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Obrir el datepicker
                DatePickerDialog dateDialog =
                        new DatePickerDialog(activity,
                                pDateSetListener,
                                pYear, pMonth, pDay);
                //Minima data seleccionable, avui:
                Calendar calendar = Calendar.getInstance();
                calendar.set(2019,5,22);
                dateDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                dateDialog.show();

            }
        });

    }

    public void sendAce(View view) {

        if ("".equals(etName.getText().toString())) {
            etName.setError(getString(R.string.err_empty));
        } else if ("".equals(etEmail.getText().toString())) {
            etEmail.setError(getString(R.string.err_empty));
        } else if ("".equals(etPhone.getText().toString())) {
            etPhone.setError(getString(R.string.err_empty));
        } else if ("".equals(etPassword.getText().toString())) {
            etPassword.setError(getString(R.string.err_empty));
        } else {
            Toast.makeText(this, "Todo OK", Toast.LENGTH_LONG).show();
        }
    }
    private void deleteContent()
    {
        etName.setText("");
        etEmail.setText("");
        etPhone.setText("");
        etPassword.setText("");
        etDate.setText("");
    }
    public void sendCan(View view)
    {

        openDialog();
    }

    public void openDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle(getString(R.string.app_name)); //modificar el titol
        alertDialogBuilder.setMessage( getString(R.string.questionDelete))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteContent();
                    }
                })
                .setNegativeButton(getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel(); // No fem res, tanquem el Alert.

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create(); //crear el alert dialog
        alertDialog.show(); //mostrar per pantalla
    }
}